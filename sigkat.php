<?php

class SIGKAT{
	
	public $apiMetadata = null;
	private $email = null;
	private $password = null;
	public $baseUrl = "http://localhost:8080/services/api";
	public $modelUrl = null;
	public $classes = null;
	public $selects = null;
	public $currentApiUser=null;
	

	function __construct($email, $password){
		$this->email = $email;
		$this->password = $password;
		$this->modelUrl=$this->baseUrl . "/model";
		$this->currentApiUser=new SIGKATEntity($this->model("SIGKATUser/currentApiUser"), $this);
	}
	
	private function initMetadata(){
		$this->apiMetadata=$this->api("model");
		$this->classes=get_object_vars($this->apiMetadata->classes);
		$this->selects=get_object_vars($this->apiMetadata->classes);
	}
	
	public function api($resource){
		return $this->getApiResource($this->baseUrl . "/".$resource);		
	}
	
	public function getApiResource($link){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $link);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_USERAGENT, 'SIGKAT_CLIENT PHP');
		if(isset($this->email) && isset($this->password)){
			curl_setopt($ch, CURLOPT_USERPWD, $this->email.":".$this->password);
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		}
		$meta = curl_exec($ch);
		curl_close($ch);
		return json_decode($meta);
	}
	
	public function model($resource){
		return $this->api("model/" . $resource);
	}	
	
	
}

class SIGKATEntity{
	private $sigkat=null;
	function __construct($json_data, $sigkat){
		$this->sigkat=$sigkat;
		$json_arr=get_object_vars($json_data);
		foreach($json_arr as $key => $value){
			$this->$key = $value;
		}
	}
}
